package com.ryanvink;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/14/13
 * Time: 8:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class DisplayTime implements State {

    SimpleClock simpleClock;

    public DisplayTime(SimpleClock simpleClock) {
        this.simpleClock = simpleClock;
    }

    @Override
    public void increment() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void decrement() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void cancel() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void changeMode() {
        this.simpleClock.setState(simpleClock.getSetHours());
    }
}
