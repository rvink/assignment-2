package com.ryanvink;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/14/13
 * Time: 7:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetMinutes implements State {

    SimpleClock simpleClock;


    public SetMinutes(SimpleClock simpleClock) {
        this.simpleClock = simpleClock;
    }

    @Override
    public void increment() {
        this.simpleClock.setMinutes(simpleClock.getMinutes() + 1, simpleClock);
    }

    @Override
    public void decrement() {
        this.simpleClock.setMinutes(simpleClock.getMinutes() - 1, simpleClock);
    }

    @Override
    public void cancel() {
        this.simpleClock.setState(simpleClock.getSetHours());
    }

    @Override
    public void changeMode() {
        this.simpleClock.setState(simpleClock.getSetSeconds());
    }
}
