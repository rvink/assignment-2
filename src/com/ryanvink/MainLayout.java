package com.ryanvink;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/13/13
 * Time: 3:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainLayout extends JFrame {
    private static final String CHANGE = "Change Mode";
    private static final String CANCEL = "Cancel";
    private static final String INCREMENT = "+";
    private static final String DECREMENT = "-";
    private JFrame jFrame;
    private JPanel contentPane;
    private JPanel topPanel;
    private JPanel bottomPanel;
    private Font font = new Font("Verdana", Font.BOLD, 128);
    private JLabel colon1 = new JLabel(":");
    private JLabel colon2 = new JLabel(":");
    private JTextArea hoursTextArea; //= new JTextArea();
    private JTextArea minutesTextArea; // = new JTextArea();
    private JTextArea secondsTextArea; // = new JTextArea();
    private JButton incButton = new JButton("+");
    private JButton decButton = new JButton("-");
    private JButton changeModeButton = new JButton("Change Mode");
    private JButton cancelButton = new JButton("Cancel");

    public MainLayout() {
        hoursTextArea = new JTextArea();
        minutesTextArea = new JTextArea();
        secondsTextArea = new JTextArea();
    }


    public void createComponents(SimpleClock simpleClock) {
        contentPane = new JPanel();
        //contentPane.setBackground(Color.GRAY);
        contentPane.setLayout(new GridLayout(2, 2));

        topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());

        hoursTextArea.setEditable(false);
        minutesTextArea.setEditable(false);
        secondsTextArea.setEditable(false);

        colon1.setFont(new Font("Verdana", Font.BOLD, 32));
        colon2.setFont(new Font("Verdana", Font.BOLD, 32));

        hoursTextArea.setFont(font);
        minutesTextArea.setFont(font);
        secondsTextArea.setFont(font);

        hoursTextArea.setColumns(1);
        minutesTextArea.setColumns(1);
        secondsTextArea.setColumns(1);

        hoursTextArea.setRows(1);
        minutesTextArea.setRows(1);
        secondsTextArea.setRows(1);

        this.setHoursTextArea(0);
        this.setMinutesTextArea(0);
        this.setSecondsTextArea(0);

        topPanel.add(hoursTextArea);
        topPanel.add(colon1);
        topPanel.add(minutesTextArea);
        topPanel.add(colon2);
        topPanel.add(secondsTextArea);

        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout());

        incButton.addActionListener(simpleClock);
        decButton.addActionListener(simpleClock);
        cancelButton.addActionListener(simpleClock);
        changeModeButton.addActionListener(simpleClock);

        incButton.setActionCommand(INCREMENT);
        decButton.setActionCommand(DECREMENT);
        cancelButton.setActionCommand(CANCEL);
        changeModeButton.setActionCommand(CHANGE);

        incButton.setVisible(false);
        decButton.setVisible(false);
        cancelButton.setVisible(false);

        bottomPanel.add(incButton);
        bottomPanel.add(decButton);
        bottomPanel.add(changeModeButton);
        bottomPanel.add(cancelButton);



        contentPane.add(topPanel);
        contentPane.add(bottomPanel);

        jFrame = new JFrame("Simple Clock");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setContentPane(contentPane);
        jFrame.pack();
        jFrame.setVisible(true);
    }

    public JButton getIncButton() {
        return this.incButton;
    }

    public JButton getDecButton() {
        return this.decButton;
    }

    public JButton getChangeModeButton() {
        return this.changeModeButton;
    }

    public JButton getCancelButton() {
        return this.cancelButton;
    }

    public void setHoursTextArea(int hour) {
        hoursTextArea.setText(String.format("%02d",hour));
    }

    public void setMinutesTextArea(int minute) {
        minutesTextArea.setText(String.format("%02d",minute));
    }

    public void setSecondsTextArea(int seconds) {
        secondsTextArea.setText(String.format("%02d",seconds));
    }
}
