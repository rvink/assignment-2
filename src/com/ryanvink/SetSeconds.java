package com.ryanvink;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/14/13
 * Time: 7:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetSeconds implements State {

    SimpleClock simpleClock;

    public SetSeconds(SimpleClock simpleClock) {
        this.simpleClock = simpleClock;
    }

    @Override
    public void increment() {
        this.simpleClock.setSeconds(simpleClock.getSeconds() + 1, simpleClock,false);
    }

    @Override
    public void decrement() {
        this.simpleClock.setSeconds(simpleClock.getSeconds() - 1, simpleClock,false);
    }

    @Override
    public void cancel() {
        this.simpleClock.setState(simpleClock.getSetMinutes());
    }

    @Override
    public void changeMode() {
        this.simpleClock.setState(simpleClock.getDisplayTime());
    }
}
