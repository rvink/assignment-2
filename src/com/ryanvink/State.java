package com.ryanvink;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/14/13
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
public interface State {

    public void increment();

    public void decrement();

    public void cancel();

    public void changeMode();
}
