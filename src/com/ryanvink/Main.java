package com.ryanvink;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.*;
import javax.swing.event.DocumentListener;

public class Main extends JFrame {

    public static void main(String[] args) {
        MainLayout mainLayout = new MainLayout();
        SimpleClock clock = new SimpleClock(mainLayout);
        mainLayout.createComponents(clock);
        Timer timer = new Timer();
        timer.schedule(new Timing(clock),1000,1000);
    }
}
