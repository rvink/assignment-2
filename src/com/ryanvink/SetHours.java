package com.ryanvink;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/14/13
 * Time: 7:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetHours implements State {

    SimpleClock simpleClock;

    public SetHours(SimpleClock simpleClock) {
        this.simpleClock = simpleClock;
    }

    @Override
    public void increment() {
        this.simpleClock.setHours(simpleClock.getHours() + 1);
    }

    @Override
    public void decrement() {
        this.simpleClock.setHours(simpleClock.getHours() - 1);
    }

    @Override
    public void cancel() {
        this.simpleClock.setState(simpleClock.getDisplayTime());
    }

    @Override
    public void changeMode() {
        this.simpleClock.setState(simpleClock.getSetMinutes());
    }
}
