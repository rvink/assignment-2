package com.ryanvink;

import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/19/13
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class Timing extends TimerTask {

    SimpleClock simpleClock;

    public Timing(SimpleClock simpleClock)
    {
        this.simpleClock = simpleClock;
        this.run();
    }

    public void run()
    {
        simpleClock.setSeconds(simpleClock.getSeconds() + 1, simpleClock,true);
    }


}
