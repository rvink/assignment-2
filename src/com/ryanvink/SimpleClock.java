package com.ryanvink;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/14/13
 * Time: 7:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleClock implements ActionListener {

    private int hours = 0;
    private int minutes = 0;
    // initialize it to -1 to kick things off from 0
    private int seconds = -1;
    State displayTime;
    State setHours;
    State setMinutes;
    State setSeconds;
    private State state;
    private MainLayout mainLayout;

    //
    public SimpleClock(MainLayout mainLayout) {
        displayTime = new DisplayTime(this);
        setHours = new SetHours(this);
        setMinutes = new SetMinutes(this);
        setSeconds = new SetSeconds(this);
        // initialize the starting state
        state = this.displayTime;
        this.mainLayout = mainLayout;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getDisplayTime() {
        return this.displayTime;
    }

    public State getSetHours() {
        return this.setHours;
    }

    public State getSetMinutes() {
        return this.setMinutes;
    }

    public State getSetSeconds() {
        return this.setSeconds;
    }

    public State getState() {
        return this.state;
    }

    public void setHours(int hours) {
        if(hours < 0) {
            this.hours = 0;
        } else {
            this.hours = hours;
        }
        mainLayout.setHoursTextArea(this.hours);
    }

    public int getHours() {
        return this.hours;
    }

    public void setMinutes(int minutes, SimpleClock simpleClock) {
        if (minutes > 59) {
            simpleClock.setHours(simpleClock.getHours() + 1);
            this.minutes = 0;
        } else if(minutes < 0) {
            this.minutes = 0;
        } else {
            this.minutes = minutes;
        }
        mainLayout.setMinutesTextArea(this.minutes);
    }

    public int getMinutes() {
        return this.minutes;
    }

    public void setSeconds(int seconds, SimpleClock simpleClock, boolean fromTimer) {
        if(fromTimer && this.state == setSeconds) {
            return;
        }
        if (seconds > 59) {
            simpleClock.setMinutes(simpleClock.getMinutes() + 1, simpleClock);
            this.seconds = 0;
        } else if(seconds < 0) {
            this.seconds = 0;
        } else {
            this.seconds = seconds;
        }
        this.mainLayout.setSecondsTextArea(this.seconds);
    }

    public int getSeconds() {
        return this.seconds;
    }

    public void actionPerformed(ActionEvent evt) {
        if ("+".equals(evt.getActionCommand())) {
            this.state.increment();
        } else if ("-".equals(evt.getActionCommand())) {
            this.state.decrement();
        } else if ("Change Mode".equals(evt.getActionCommand())) {
            this.mainLayout.getCancelButton().setVisible(true);
            this.mainLayout.getIncButton().setVisible(true);
            this.mainLayout.getDecButton().setVisible(true);
            this.state.changeMode();
            if(this.state == displayTime) {
                this.mainLayout.getCancelButton().setVisible(false);
                this.mainLayout.getIncButton().setVisible(false);
                this.mainLayout.getDecButton().setVisible(false);
            }
        } else if ("Cancel".equals(evt.getActionCommand())) {
            this.state.cancel();
            if(this.state == displayTime) {
                this.mainLayout.getCancelButton().setVisible(false);
                this.mainLayout.getIncButton().setVisible(false);
                this.mainLayout.getDecButton().setVisible(false);
            }
        }
    }
}
